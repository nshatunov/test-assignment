describe('Jasmine', function () {
	it('should work', function() {
		expect(true).toBe(true);
	})
})

describe('AddressBookController', function () {
	var scope, controller, mockContactData;
	var store = [];

	beforeEach(function() {
		module('addressBookApp');
		
		// mock localStorage
		spyOn(localStorage, 'getItem').andCallFake(function() {
			return store;
		})

		spyOn(localStorage, 'setItem').andCallFake(function(value) {
			store = value;
		})

		store = [
			{firstName: 'John', lastName: 'Doe', phone: '+372555555', group: ''},
			{firstName: 'Jane', lastName: 'Doe', phone: '+372222222', group: 'work'}
		];

		localStorage.setItem(store);

		mockContactData = {
			get: function() {
				return localStorage.getItem();
			},
			clearAll: function() {
				localStorage.setItem([]);
			}
		}

		// instantiate AddressBookController
		inject(function($controller, $rootScope){
			scope = $rootScope.$new();
			controller = $controller('AddressBookController', {
				$scope: scope, contactData: mockContactData
			})
		});

		// bootstrap modal call mock
		spyOn($.fn, 'modal').andReturn('modal disabled');

		// contactForm mock
		scope.contactForm = {
			$valid: true,
			$setPristine: function() {
				return;
			}
		};

	});

	afterEach(function() {
		store = [];
	});

	it('should retrieve contacts from local storage', function() {		
		expect(scope.contacts).toBe(store);
	});

	it('should remove contact with scope.remove method', function() {
		var contact = scope.contacts[0];
		expect(contact.firstName).toBe('John');
		expect(scope.contacts.length).toBe(2);
		scope.remove(contact);
		expect(scope.contacts.length).toBe(1);
	});

	it('should add a contact to scope.contacts', function() {
		var contact = {
			firstName: 'Michael',
			lastName: 'Moore',
			phone: '+333333333'
		};

		scope.contact = contact;

		scope.saveContact(contact);

		expect(scope.contacts.length).toBe(3);

	});

	it('should change contacts first name', function() {
		var contact = scope.contacts[0];
		contact.firstName = 'Foo';
		scope.edit(contact);
		scope.saveContact(contact);
		expect(scope.contacts.length).toBe(2);
		expect(scope.contacts[0].firstName).toBe('Foo');
	});

	it('should clear all contacts from scope', function() {
		scope.clearAll();
		expect(scope.contacts.length).toBe(0);
	});
});

describe('ContactData service', function() {
	var store = [];
	beforeEach(function() {
		module('addressBookApp');

		// mock localStorage
		spyOn(localStorage, 'getItem').andCallFake(function(storageID) {
			return store;
		});

		spyOn(localStorage, 'setItem').andCallFake(function(storageID, data) {
			store = data;
		});

		var contacts = [
			{firstName: 'John', lastName: 'Doe', phone: '+372555555', group: ''},
			{firstName: 'Jane', lastName: 'Doe', phone: '+372222222', group: 'work'}
		];

		localStorage.setItem('', JSON.stringify(contacts));

	})

	afterEach(function() {
		store = [];
	});

	it('should get data from localStorage', inject(function(contactData) {
		var contacts = contactData.get();
		expect(contacts).toBeDefined();
		expect(contacts.length).toBe(2);
	}));

	it('should clear localStorage', inject(function(contactData){

		var contacts = contactData.get();
		expect(contacts).toBeDefined();
		expect(contacts.length).toBe(2);

		contactData.clearAll();
		contacts = contactData.get();		
		// console.log(contactData);
		expect(contacts.length).toBe(0);

	}));

	it('should add data to localStorage', inject(function(contactData) {
		var contacts = contactData.get();
		expect(contacts.length).toBe(2);
		contacts.pop();
		expect(contacts.length).toBe(1);

		contactData.put(contacts);
		contacts = contactData.get()		
		expect(contacts.length).toBe(1);

	}));
});