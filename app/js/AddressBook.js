'use strict';
var addressBookApp = angular.module('addressBookApp', []);

addressBookApp.controller('AddressBookController', 
	function AddressBookController ($scope, contactData) {
		//contacts container
		var contacts = $scope.contacts = contactData.get(); 

		$scope.showErrors = false; // show invalid fields if contact form doesent validate
		$scope.isNew = true; // check if update or insert
		$scope.contactsExist = contacts.length > 0; // used to display apropriate message if no records in contacts array
		
		$scope.groups = ['family', 'friends', 'work']; 
		$scope.contact = {
			firstName: '',
			lastName: '',
			phone: '',
			group: ''
		};

		//watch for any changes in contacts and update local storage if neccessary
		$scope.$watch('contacts', function (newValue, oldValue) {
			if (newValue !== oldValue) {
				contactData.put(contacts);			
				$scope.contactsExist = contacts.length > 0;
			}
		}, true);

		// updates or inserts contact
		$scope.saveContact = function (contact) {
			var clearContact = {
				firstName: '',
				lastName: '',
				phone: '',
				group: ''
			};

			console.log($scope.contactForm);
			if($scope.contactForm.$valid) {
				if($scope.isNew) contacts.push(angular.copy(contact)); //insert
				else contacts[contacts.indexOf(contact)] = contact; // update

				// reset form
				$scope.contactForm.$setPristine();
				$scope.contact = clearContact;

				$('#newContact').modal('hide');
				$scope.isNew = true;
				$scope.showErrors = false;
			} else {
				$scope.showErrors = true; // show invalid fields
			}
		};

		// remove contact
		$scope.remove = function(contact) {
			contacts.splice(contacts.indexOf(contact), 1);
		};

		// prepare contacForm for update event
		$scope.edit = function (contact) {
			$scope.isNew = false;
			$scope.contact = contact;	
			$('#newContact').modal('show');
		};

		// delete all contacts
		$scope.clearAll = function() {
			contactData.clearAll(); 
			contacts = $scope.contacts = [];
			$scope.contactsExist = contacts.length > 0;			
		}
	}
);

// Contact Data service returns array of contacts from localStorage
addressBookApp.factory('contactData', function () {
	var STORAGE_ID = 'address-book';
		return {
			get: function () {
				return JSON.parse(localStorage.getItem(STORAGE_ID) || '[]');
			},
			put: function (contacts) {
				localStorage.setItem(STORAGE_ID, JSON.stringify(contacts));
			},
			clearAll: function () {
				localStorage.setItem(STORAGE_ID, '');
			}
		};
});

// Confirm click directive for risky click actions
addressBookApp.directive('ngConfirmClick', [function() {
	return {
		restrict: 'A',
		link: function(scope, element, attrs) {
			element.bind('click', function() {
				var condition = scope.$eval(attrs.ngConfirmCondition);
				if(condition){
					var message = attrs.ngConfirmMessage;
					if (message && confirm(message)) {
						scope.$apply(attrs.ngConfirmClick);
					}
				}
				else{
					scope.$apply(attrs.ngConfirmClick);
				}
			});
		}
	}
}]);